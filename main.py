#!/usr/bin/env python

import funnyfuncs as ff

def main():
    print("3 + 5 = {0}".format(ff.add(3,5)))
    print("3 - 5 = {0}".format(ff.sub(3,5)))
    print("3 * 5 = {0}".format(ff.mul(3,5)))
    print("3 / 5 = {0}".format(ff.div(3,5)))

if __name__ == '__main__':
    main()
