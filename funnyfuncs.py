#!/usr/bin/env python

def add(a, b):
    return a+b

def sub(a, b):
    return a-b

def mul(a, b):
    return a*b

def div (a, b):
    return a/b

class TestFunnyfuncs:
    def test_add(self):
        assert add(3, 5) == 8
        assert add(5, 3) == 8
        assert add(-2, 2) == 0

    def test_sub(self):
        assert sub(5, 3) == 2
        assert sub(3, 5) == -2
        assert sub(5, 5) == 0
        assert sub(5, -5) == 10

    def test_mul(self):
        assert mul(5, 3) == 15
        assert mul(3, 5) == 15
        assert mul(-3, 5) == -15
        assert mul(-3, -5) == 15
        assert mul(5, 0) == 0

    def test_div(self):
        assert div(3, 5) == 0.6
        assert div(12, 3) == 4
        assert div(-3, 5) == -0.6
        assert div(-3, -5) == 0.6
